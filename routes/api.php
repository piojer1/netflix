<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('movie/createmovie', 'MovieController@create');
Route::get('movie/getmovie/{movie}', 'MovieController@read');
Route::delete('movie/deletemovie/{movie}', 'MovieController@delete');
Route::put('movie/updatemovie/{movie}', 'MovieController@update');
Route::get('movie/getmovies/', 'MovieController@readList');
Route::get('movie/getmoviesbydirector/', 'MovieController@readListByDirector');
Route::post('eksport', 'MovieController@eksport');

