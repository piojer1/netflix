<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Movie;

class Test extends TestCase
{
    public function test_create()
    {
        $response = $this->postJson('/api/movie/createmovie', ["name" =>"5", "description" =>"test", "director" =>"testD", "production_date" =>"2020-10-20"]);

        $response->assertStatus(200);
    }
    public function test_read()
    {
        $response = $this->getJson('/api/movie/getmovie/'.Movie::inRandomOrder()->first()->id);

        $response->assertStatus(200);
    }
    public function test_update()
    {
        $response = $this->putJson('/api/movie/updatemovie/'.Movie::inRandomOrder()->first()->id, ["name" =>"test"]);

        $response->assertStatus(200);
    }
    public function test_delete()
    {
        $response = $this->deleteJson('/api/movie/deletemovie/'.Movie::inRandomOrder()->first()->id);

        $response->assertStatus(200);
    }
    public function test_readList()
    {
        $response = $this->getJson('/api/movie/getmovies/');

        $response->assertStatus(200);
    }
    public function test_readListByDirector()
    {
        $response = $this->getJson('/api/movie/getmoviesbydirector', ["director"=>"testDirec"]);

        $response->assertStatus(200);
    }
}
