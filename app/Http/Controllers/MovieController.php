<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\GenreMovie;
use App\Models\Movie;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function create(Request $request){
       $movie = Movie::create($request->all());
       $movie->genres()->attach($request->genres);
        return 200;

    }
    public function read(Movie $movie){
        return $movie;

    }
    public function update(Movie $movie, Request $request){
        $movie->update($request->all());
        $movie->genres()->sync($request->genres);
        return $movie;

    }
    public function delete(Movie $movie){
        $movie->delete();
        return 200;
    }
    public function readList(){
        return Movie::all();
    }
    public function readListByDirector(Request $request){
         return Movie::where('director', $request->director)
         ->get();
    }
    public function eksport(){
         return ["movies" => Movie::all(), "genres" => Genre::all(), "genremovies" => GenreMovie::all()];
    }

}
