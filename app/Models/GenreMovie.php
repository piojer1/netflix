<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GenreMovie extends Model
{
    protected $fillable = ['genre_id', 'movie_id'];
    protected $table = "genres_movies";
    use HasFactory;
}
